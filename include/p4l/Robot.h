/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 10/31/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Robot class wrapper for ros functions
 */

#ifndef ROBOT_H
#define ROBOT_H

#include <ros/ros.h>

class Robot
{
public:
    /** \brief Register publishers and wait for subsriber */
    Robot();

    /** \brief Sleep for specified number of seconds 
     *  \param seconds Number of seconds to sleep */
    void sleep ( double seconds );

    /** \brief Controll robot velocity 
     *  \param forward Forward (ahead) velocity
     *  \param angular Angular (rotational) velocity */
    void set_velocity ( double forward = 0.0, double angular = 0.0 );

private:
    ros::NodeHandle node;
    ros::Publisher pub_cmd_vel;
};

#endif // ROBOT_H
