/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 08/21/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Robot moving example using the cmd_vel publisher
 * 
 *             How to run in simulation:
 *               roslaunch p4l simulator_stdr.launch
 *               rosrun p4l p4l_cmd_vel
 */

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

int main(int argc, char** argv) {
    ros::init(argc, argv, "p4l_cmd_vel");  // Register node into the ROS with name "p4l_cmd_vel"
    ros::NodeHandle node("~");             // Create node handle which can access private parameters

    ros::Publisher pub_cmd;                // Publisher is object which send data to the particular topic
    pub_cmd = node.advertise<geometry_msgs::Twist>("/cmd_vel", 1); //advertise publisher to topic "cmd_vel"
    
    ros::Duration(0.5).sleep();            // Sleep for half second - give publisher time to advertise
    
    ROS_INFO("Move robot");                // Print message with severity "INFO"
    geometry_msgs::Twist msg;              // Create message which will be sent via topic
    msg.linear.x = 0.5;                    // Lineaer (forward) velocity
    msg.angular.z = 0.1;                   // Angular (rotational) velocity
    pub_cmd.publish(msg);                  // Publish - send data via the topic
    
    ros::Duration(5).sleep();              // wait for 5second

    ROS_INFO("Stop robot");
    msg.linear.x = 0.0;
    msg.angular.z = 0.0;
    pub_cmd.publish(msg);

    ros::Duration(0.5).sleep();
    
    return 0;                              // Program end
}
