/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 10/31/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: First programming P4L lecture template
 */

#include <p4l/Robot.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "lecture_1");
    
    Robot r;
    
//     r.set_velocity(0.5, 0.0);
//     r.sleep(0.6);
    
    return 0;
}
