#include <p4l/Robot.h>
#include <geometry_msgs/Twist.h>

Robot::Robot() :
    node("~")
{
    pub_cmd_vel = node.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
    
    while(ros::ok()) {
        if(pub_cmd_vel.getNumSubscribers() > 0) {
            break;
        }
        ros::Duration(0.05).sleep();
        ROS_INFO_STREAM("Waiting for subsribers for cmd_vel topic - is robot running?");
    }
}

void Robot::set_velocity(double forward, double angular)
{
    geometry_msgs::Twist msg;
    msg.linear.x = forward;
    msg.angular.z = angular;
    pub_cmd_vel.publish(msg);
}

void Robot::sleep(double seconds)
{
    ros::Duration(seconds).sleep();
}
